//
//  ViewController.m
//  MobileCardboardIOS
//
//  Created by Jeremy Lightsmith on 05/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import <CoreMotion/CoreMotion.h>
#import <QuartzCore/QuartzCore.h>
#import "ViewController.h"

#define PI 3.14159265
float midX = 320 / 2;
float midY = 480 / 2;


@interface ViewController ()

@end

@implementation ViewController {
  CGSize originalSize;
  CGPoint origin;
  id displayLink;
  CMMotionManager *_motionManager;
  float last_yaw;
  float last_roll;
  float last_pitch;
  float oldScale;
}
@synthesize zoomSlider;
@synthesize imageView;
@synthesize scrollView;
@synthesize originalSize;
@synthesize origin;
@synthesize displayLink;
@synthesize motionManager = _motionManager;


- (void)startMotion {
  _motionManager.showsDeviceMovementDisplay = true;

  if (_motionManager.deviceMotionAvailable && !_motionManager.deviceMotionActive) {
    _motionManager.deviceMotionUpdateInterval = 1.0 / 50.0;
    [_motionManager startDeviceMotionUpdates];
//    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
//    [motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMDeviceMotion *motion, NSError *error) {
//      NSLog(@"food");
//      float pitch = motion.attitude.pitch;
//      float roll = motion.attitude.roll;
//      float yaw = motion.attitude.yaw;
//
//      NSLog(@"ROTATION: x:%f y:%f z:%f", pitch, roll, yaw);
//    }];
  }
}

- (void)onDisplayLink:(id)onDisplayLink {
  CMDeviceMotion *d = _motionManager.deviceMotion;

  CMAttitude *attitude = _motionManager.deviceMotion.attitude;
  float pitch = attitude.pitch;
  float roll = attitude.roll;
  float yaw = attitude.yaw;
  float threshold = 0.001;

//  NSLog(@"x,y=%0.2f,%02f, threshhold=%0.2f", roll, pitch, threshold);
  float deltaX = (last_roll - roll);
  float deltaY = (last_pitch - pitch);

  if (pow(deltaX, 2) > pow(1.5 * PI, 2)) {
    deltaX = 0;
  }
  if (pow(deltaY, 2) > pow(1.5 * PI, 2)) {
    deltaY = 0;
  }

  if (pow(deltaX, 2) < pow(threshold, 2)) {
    deltaX = 0;
  }
  if (pow(deltaY, 2) < pow(threshold, 2)) {
    deltaY = 0;
  }

//  imageView.center +=
  imageView.center = CGPointMake(imageView.center.x + deltaX * 300, imageView.center.y + deltaY * 300);

  last_pitch = pitch;
  last_roll = roll;
  last_yaw = yaw;
  
//  NSLog(@"ROTATION: x:%f y:%f z:%f", pitch, roll, yaw);

  if (d != nil) {
    CMRotationMatrix r = d.attitude.rotationMatrix;
//    transformFromCMRotationMatrix(cameraTransform, &r);
//    [self setNeedsDisplay];
  }
}

- (void)startDisplayBoard {
  displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(onDisplayLink:)];
  [displayLink setFrameInterval:1];
  [displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

- (void)startGyro {
  if([self.motionManager isGyroAvailable])
  {
    /* Start the gyroscope if it is not active already */
    if([self.motionManager isGyroActive] == NO)
    {
      /* Update us 2 times a second */
      [self.motionManager setGyroUpdateInterval:1.0f / 2.0f];

      /* And on a handler block object */

      /* Receive the gyroscope data on this block */
      [self.motionManager startGyroUpdatesToQueue:[NSOperationQueue mainQueue]
          withHandler:^(CMGyroData *gyroData, NSError *error)
                      {
                        NSString *x = [[NSString alloc] initWithFormat:@"%.02f",gyroData.rotationRate.x];
                        NSString *y = [[NSString alloc] initWithFormat:@"%.02f",gyroData.rotationRate.y];
                        NSString *z = [[NSString alloc] initWithFormat:@"%.02f",gyroData.rotationRate.z];

//                        NSLog(@"%@, %@, %@", x, y, z);
                      }];
    }
  }
  else
  {
    NSLog(@"Gyroscope not Available!");
  }
}

- (void)viewDidLoad {
  [super viewDidLoad];
  zoomSlider.minimumValue = scrollView.minimumZoomScale = 0.1;
  zoomSlider.maximumValue = scrollView.maximumZoomScale = 0.8;
  originalSize = imageView.image.size;
  imageView.bounds = CGRectMake(0, 0, originalSize.width, originalSize.height);

  _motionManager = [[CMMotionManager alloc] init];
  [self startMotion];
  [self startGyro];
  [self startDisplayBoard];
//  [[UIAccelerometer sharedAccelerometer] setUpdateInterval:1.0/30.0];
//  [[UIAccelerometer sharedAccelerometer] setDelegate:self];

}

- (void)viewDidUnload {
  [self setZoomSlider:nil];
  [self setScrollView:nil];
  [self setImageView:nil];
  [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
//        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
//    } else {
//        return NO;
//    }
  return NO;
  
}

- (IBAction)reset:(id)sender {
  imageView.center = CGPointMake(midX, midY);
}

- (IBAction)zoomed:(UISlider *)sender {
  float scale = sender.value;
  
  float ratio = scale / oldScale;

//  [CATransaction begin];
//  [CATransaction setDisableActions:TRUE];

//  [scrollView setZoomScale:scale animated:true];
  float x = (imageView.frame.origin.x - midX) * ratio + midX;
  float y = (imageView.frame.origin.y - midY) * ratio + midY;
//  float x = (imageView.center.x - 150) * ratio + 150;
//  float y = (imageView.center.y - 150) * ratio + 150;
//    imageView.center = CGPointMake(imageView.center.x - 150, imageView.center.y - 150);
  
  

  imageView.frame = CGRectMake(x, y, scale * originalSize.width, scale * originalSize.height);
//  imageView.bounds = CGRectMake(x, y, scale * originalSize.width, scale * originalSize.height);
//  imageView.center = CGPointMake(imageView.center.x * ratio, imageView.center.y * ratio);
//  imageView.center = CGPointMake(imageView.center.x + 150, imageView.center.y + 150);

  
//  UIScrollView *scrollView = (UIScrollView*)[self superview];
//  
//  CGRect newFrame = imageView.frame;
//  newFrame.size.width *= scale;
//  newFrame.size.height *= scale;
//  
//  scrollView.contentOffset = CGPointMake(scrollView.contentOffset.x - scrollView.zoomScale * newFrame.origin.x, 
//                                         scrollView.contentOffset.y - scrollView.zoomScale * newFrame.origin.y);
//  
//  self.bounds = CGRectMake(x, y, newFrame.size.width, newFrame.size.height);
//  [self setNeedsDisplay];

  
//  [CATransaction commit];
  oldScale = scale;
}

-(void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
  origin = CGPointMake(acceleration.x, acceleration.y);

  double magnitude = pow(acceleration.x, 2) + pow(acceleration.y, 2) + pow(acceleration.z, 2);
  
  
//  NSLog(@"magnitude = %f", magnitude);
//  if (magnitude < 1) return; // skip

//  imageView.center = CGPointMake(imageView.center.x + origin.x, imageView.center.y + origin.y);

//    int newX = (int)(ball.center.x + valueX);
//
//  if (newX > 320-BALL_RADIUS)
//    newX = 320-BALL_RADIUS;
//
//  if (newX < 0 + BALL_RADIUS)
//    newX = 0 + BALL_RADIUS;
//
//  int newY = (int)(ball.center.y-valueY);
//
//  if (newY > 460 – BALL_RADIUS)
//  newY = 460 – BALL_RADIUS;
//
//  if (newY < 0 + BALL_RADIUS)
//    newY = 0 + BALL_RADIUS;
//
//  CGPoint newCenter = CGPointMake(newX, newY);
//  ball.center = newCenter;

}


@end