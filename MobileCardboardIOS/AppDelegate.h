//
//  AppDelegate.h
//  MobileCardboardIOS
//
//  Created by Jeremy Lightsmith on 05/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end