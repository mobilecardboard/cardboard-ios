//
//  ViewController.h
//  MobileCardboardIOS
//
//  Created by Jeremy Lightsmith on 05/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CMMotionManager;

@interface ViewController : UIViewController<UIAccelerometerDelegate>
@property (weak, nonatomic) IBOutlet UISlider *zoomSlider;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property(nonatomic, assign) CGSize originalSize;
@property(nonatomic, assign) CGPoint origin;
@property(nonatomic, strong) id displayLink;
@property(nonatomic, strong) CMMotionManager *motionManager;


- (IBAction)reset:(id)sender;
- (IBAction)zoomed:(UISlider *)sender;

@end